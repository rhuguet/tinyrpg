from .character import Character
from .weapon import Weapon
import random


class World:

    def __init__(self, width, height, npcs, hero, weapons):
        self._set_width(width)
        self._set_height(height)

        self._weapons = weapons
        self._set_hero(hero)
        self._set_npcs(npcs)
        self._positions = {}

    def _set_width(self, width):
        if not width:
            raise AttributeError('World width can\'t be 0')
        self._width = width

    def _set_height(self, height):
        if not height:
            raise AttributeError('World height can\'t be 0')
        self._height = height

    def _set_npcs(self, npcs):
        if len(npcs) == 0:
            raise AttributeError('NPCs can\'t be empty')
        self._npcs = npcs

    def _set_hero(self, hero):
        if not isinstance(hero, Character):
            raise AttributeError('hero must be an instance of Character')
        self._hero = hero

    def _random_coord(self):
        return (random.randint(1, self._width), random.randint(1, self._height))

    def set_random_initial_positions(self):
        for a in self._artifacts:
            self._positions[a] = self._random_coord()

    @property
    def _artifacts(self):
        artifacts = self._npcs + self._weapons
        artifacts.append(self._hero)
        return artifacts

    def find_artifacts(self, x, y):
        artifacts = []
        for k, v in self._positions.items():
            if v == (x, y):
                artifacts.append(k)
        return artifacts
    
    def validate_command_for(character, action):
        pass

    def change_position_of(self, character, add_x=0, add_y=0):
        next_position = (self._positions[character][0] + add_x, self._positions[character][1] + add_y)
        if next_position[0] < 1 or next_position[0] > self._width or next_position[1] < 1 or next_position[1] > self._height:
            raise AttributeError('Action is not possible.')
        self._positions[character] = next_position

    def process_attack(self, attacker):
        attacker_position = self._positions[attacker]
        defenders = []
        for a in self.find_artifacts(x=attacker_position[0], y=attacker_position[1]):
            if isinstance(a, Character) and a != attacker:
                defenders.append(a)
        if len(defenders) == 0:
            raise AttributeError('Nothing to attack')
        damage = attacker.attack()
        for defender in defenders:
            defender.take_damages(damage)

    def change_weapon(self, character):
        position = self._positions[character]
        weapons = []
        for a in self.find_artifacts(x=position[0], y=position[1]):
            if isinstance(a, Weapon):
                weapons.append(a)
        if len(weapons) == 0:
            raise AttributeError('Nothing to pick up')
        old_weapon = character.change_weapon(new_weapon=weapons[0])
        self._positions.pop(weapons[0])
        self._weapons.pop(self._weapons.index(weapons[0]))
        if old_weapon:
            self._positions[old_weapon] = position
            self._weapons.append(old_weapon)

    def update(self):
        self._hero.heal(1)
        for npc in self._npcs:
            npc.heal(1)
            if not npc.is_alive:
                self._positions.pop(npc)
                self._npcs.pop(self._npcs.index(npc))

    @property
    def npcs_remaining(self):
        for npc in self._npcs:
            if npc.is_alive:
                return True
        return False

    @property
    def did_hero_win_or_lose(self):
        print(self._hero.is_alive)
        print(self.npcs_remaining)
        return not self._hero.is_alive or not self.npcs_remaining

    def __repr__(self):
        return 'World {L}x{W} with {npcs} Npcs, hero {hero} and {weapons} weapons.'.format(
            L=self._height,
            W=self._width,
            hero=self._hero,
            npcs=len(self._npcs),
            weapons=len(self._weapons)
        )
