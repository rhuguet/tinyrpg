from .weapon import Weapon


class Character:
    MAX_HEALTH = 100

    def __init__(self, bad_guy=True):
        self.health = self.MAX_HEALTH
        self.weapon = None
        self.bad_guy = bad_guy

    def change_weapon(self, new_weapon):
        if not isinstance(new_weapon, Weapon):
            raise AttributeError('new_weapon must be a Weapon')
        old_weapon = self.weapon
        self.weapon = new_weapon
        return old_weapon

    def take_damages(self, damages):
        self.health -= damages
    
    def heal(self, points):
        self.health += points

    def attack(self):
        if not self.weapon:
            return 0
        else:
            self.take_damages(self.weapon.auto_damages)
            return self.weapon.damages

    @property
    def is_alive(self):
        return self.health > 0

    def __repr__(self):
        return 'Character with {health} health points and weapon {w}'.format(health=self.health, w=self.weapon)

    def __str__(self):
        return 'X' if self.bad_guy else 'O'