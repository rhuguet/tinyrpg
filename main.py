#! /usr/bin/env python3
import domain
from domain.services import attack, pick_up, move_up, move_down, move_left, move_right
import random

BASIC_MAP_WIDTH = 5
BASIC_MAP_HEIGHT = 5

USER_COMMANDS = {
    1: move_up,
    2: move_down,
    3: move_left,
    4: move_right,
    5: attack,
    6: pick_up,
}


def to_repre(artifacts, size):
    repre = ''.join([str(a) for a in artifacts])
    if len(artifacts) < size:
        repre += ''.join([' ' for i in range(1, size-len(artifacts))])
    return repre


def display_actions():
    print('What do you want to do next turn?')
    print('1: Move up')
    print('2: Move down')
    print('3: Move right')
    print('4: Move left')
    print('5: Attack a bad guy on your position')
    print('6: Pick up the weapon on your position')


def display_map(world):
    lines = [[
        to_repre(world.find_artifacts(x, y), 4) for x in range(1, world._width + 1)
    ] for y in range(1, world._height + 1)]

    print('--------------------------')
    print('Here is the map !')
    for line in lines:
        print('|'.join(line))
        print(''.join(['-' for i in range(4*(world._width))]))

    print('X are bad guys, W are weapons, O is you!')


def display_status(world):
    print('Hero status: health {h}'.format(h=world._hero.health))
    if world._hero.weapon:
        print('Hero has weapon {wd}/{wad}'.format(wd=world._hero.weapon.damages, wad=world._hero.weapon.auto_damages))
    for npc in world._npcs:
        print('NPC status: health {h} - Weapon {wd}/{wad}'.format(h=npc.health, wd=npc.weapon.damages, wad=npc.weapon.auto_damages))


def initialize_world():
    hero = domain.Character(bad_guy=False)
    weapons = [
        domain.Weapon(damages=5, auto_damages=1),
        domain.Weapon(damages=4),
        domain.Weapon(damages=8, auto_damages=3)
    ]
    npc1 = domain.Character()
    npc1.change_weapon(new_weapon=weapons.pop(random.randint(0, 2)))
    npc2 = domain.Character()
    npc2.change_weapon(new_weapon=weapons.pop(random.randint(0, 1)))

    world = domain.World(width=BASIC_MAP_WIDTH, height=BASIC_MAP_WIDTH, npcs=[npc1, npc2], hero=hero, weapons=weapons)
    world.set_random_initial_positions()
    return world


def system_plays(world):
    for npc in world._npcs:
        try:
            world.process_attack(attacker=npc)
        except AttributeError:
            print('NPC {n} does nothing'.format(n=npc))


if __name__ == '__main__':
    print('Welcome in very simple RPG')
    print('--------------------------')
    my_input = input('Press enter to start the game\n')

    world = initialize_world()
    display_map(world)
    while world._hero.is_alive and world.npcs_remaining:
        display_actions()
        try:
            action = int(input('Enter the number corresponding to your choice!\n'))
            print('--------------------------')
            print('---------Playing----------')
            USER_COMMANDS[action](world)
        except (AttributeError, ValueError):
            print('You can\'t do that!')
            continue
        system_plays(world)
        world.update()
        display_status(world)
        display_map(world)
