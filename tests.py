#! /usr/bin/env python3
import unittest
from domain import World, Character, Weapon


class TestWeapon(unittest.TestCase):

    def test_can_instantiate_a_weapon(self):
        w1 = Weapon(damages=10, auto_damages=2)
        self.assertIsInstance(w1, Weapon)
        self.assertEqual(w1.damages, 10)
        self.assertEqual(w1.auto_damages, 2)


class TestCharacter(unittest.TestCase):

    def test_character_can_be_instantiated_with_max_health_and_no_weapon(self):
        character = Character()
        self.assertIsInstance(character, Character)
        self.assertEqual(character.health, Character.MAX_HEALTH)
        self.assertIsNone(character.weapon)

    def test_cant_change_weapon_if_new_weapon_is_not_a_weapon(self):
        character = Character()
        with self.assertRaises(AttributeError):
            character.change_weapon(new_weapon={})

    def test_attack_without_weapon(self):
        character = Character()
        r = character.attack()
        self.assertEqual(character.health, Character.MAX_HEALTH)
        self.assertEqual(r, 0)

    def test_attack_with_weapon(self):
        character = Character()
        weapon = Weapon(damages=10, auto_damages=5)
        character.change_weapon(weapon)
        r = character.attack()
        self.assertEqual(character.health, Character.MAX_HEALTH - 5)
        self.assertEqual(r, 10)

    def test_get_previous_weapon_when_changing_weapon(self):
        w1 = Weapon(damages=1)
        w2 = Weapon(damages=2)
        character = Character()
        r1 = character.change_weapon(w1)
        self.assertEqual(character.weapon, w1)
        self.assertIsNone(r1)
        r2 = character.change_weapon(w2)
        self.assertEqual(character.weapon, w2)
        self.assertEqual(r2, w1)


class TestWorld(unittest.TestCase):

    def test_world_cant_be_created_with_improper_hero(self):
        with self.assertRaises(AttributeError):
            World(width=20, height=20, npcs=[Character()], hero={}, weapons={})

    def test_world_cant_be_created_without_npcs(self):
        with self.assertRaises(AttributeError):
            World(width=20, height=20, npcs=[], hero=Character(), weapons={})

    def test_world_cant_be_a_point(self):
        with self.assertRaises(AttributeError):
            World(width=0, height=0, npcs=[Character()], hero=Character(), weapons={})

    def test_world_cant_be_a_line(self):
        with self.assertRaises(AttributeError):
            World(width=0, height=50, npcs=[Character()], hero=Character(), weapons={})
        with self.assertRaises(AttributeError):
            World(width=50, height=0, npcs=[Character()], hero=Character(), weapons={})

    def test_world_can_be_instantiate(self):
        world = World(width=20, height=20, npcs=[Character()], hero=Character(), weapons={})
        self.assertIsInstance(world, World)

    def test_world_set_random_positions(self):
        world = World(width=20, height=20, npcs=[Character()], hero=Character(), weapons=[Weapon(damages=5)])
        world.set_random_initial_positions()
        self.assertIsNotNone(world._positions)
        self.assertIsInstance(world._positions, dict)
        for p in world._positions.values():
            self.assertIsInstance(p, tuple)
            self.assertGreaterEqual(p[0], 0)
            self.assertGreaterEqual(p[1], 0)
            self.assertLessEqual(p[0], world._width)
            self.assertLessEqual(p[1], world._height)

    def test_find_artifacts(self):
        hero = Character()
        npc = Character()
        world = World(width=20, height=20, npcs=[npc], hero=hero, weapons=[Weapon(damages=5)])
        world._positions[npc] = (5, 6)
        world._positions[hero] = (10, 8)
        self.assertListEqual([], world.find_artifacts(1, 1))
        self.assertListEqual([hero], world.find_artifacts(10, 8))
        self.assertListEqual([npc], world.find_artifacts(5, 6))
        world._positions[hero] = (5, 6)
        self.assertListEqual([npc, hero], world.find_artifacts(5, 6))

    def test_cant_change_position_of_character_outside_of_the_map(self):
        hero = Character()
        npc = Character()
        world = World(width=5, height=5, npcs=[npc], hero=hero, weapons=[Weapon(damages=5)])
        world._positions[npc] = (1, 1)
        with self.assertRaises(AttributeError):
            world.change_position_of(character=npc, add_x=-1)
        with self.assertRaises(AttributeError):
            world.change_position_of(character=npc, add_x=7)
        with self.assertRaises(AttributeError):
            world.change_position_of(character=npc, add_y=-2)

    def test_change_position_of_character(self):
        hero = Character()
        npc = Character()
        world = World(width=5, height=5, npcs=[npc], hero=hero, weapons=[Weapon(damages=5)])
        world._positions[npc] = (1, 1)
        world.change_position_of(character=npc, add_x=2)
        self.assertEqual(world._positions[npc], (3, 1))
        world.change_position_of(character=npc, add_y=1)
        self.assertEqual(world._positions[npc], (3, 2))
        world.change_position_of(character=npc, add_y=1, add_x=-1)
        self.assertEqual(world._positions[npc], (2, 3))

    def test_cant_attack_if_no_defender(self):
        hero = Character()
        npc = Character()
        world = World(width=5, height=5, npcs=[npc], hero=hero, weapons=[Weapon(damages=5)])
        world._positions[npc] = (1, 1)
        world._positions[hero] = (1, 2)
        with self.assertRaises(AttributeError):
            world.process_attack(attacker=npc)
    
    def test_process_attack(self):
        w1 = Weapon(damages=2)
        w2 = Weapon(damages=4, auto_damages=1)
        hero = Character()
        hero.change_weapon(w1)
        npc = Character()
        npc.change_weapon(w2)
        world = World(width=5, height=5, npcs=[npc], hero=hero, weapons=[Weapon(damages=5)])
        world._positions[npc] = (1, 1)
        world._positions[hero] = (1, 1)
        world.process_attack(attacker=npc)
        self.assertEqual(hero.health, Character.MAX_HEALTH - 4)
        self.assertEqual(npc.health, Character.MAX_HEALTH - 1)
        world.process_attack(attacker=hero)
        self.assertEqual(hero.health, Character.MAX_HEALTH - 4)
        self.assertEqual(npc.health, Character.MAX_HEALTH - 3)

    def test_cant_change_weapon(self):
        w1 = Weapon(damages=2)
        w2 = Weapon(damages=4, auto_damages=1)
        w3 = Weapon(damages=5)
        hero = Character()
        hero.change_weapon(w1)
        npc = Character()
        npc.change_weapon(w2)
        world = World(width=5, height=5, npcs=[npc], hero=hero, weapons=[w3])
        world._positions[npc] = (1, 1)
        world._positions[hero] = (1, 1)
        world._positions[w3] = (1, 2)
        with self.assertRaises(AttributeError):
            world.change_weapon(character=hero)

    def test_change_weapon(self):
        w1 = Weapon(damages=2)
        w2 = Weapon(damages=4, auto_damages=1)
        w3 = Weapon(damages=5)
        hero = Character()
        hero.change_weapon(w1)
        npc = Character()
        npc.change_weapon(w2)
        world = World(width=5, height=5, npcs=[npc], hero=hero, weapons=[w3])
        world._positions[npc] = (1, 1)
        world._positions[hero] = (1, 1)
        world._positions[w3] = (1, 1)
        world.change_weapon(character=hero)
        self.assertEqual(hero.weapon, w3)
        self.assertEqual(world._positions[w1], (1, 1))



if __name__ == '__main__':
    unittest.main()
