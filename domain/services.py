def move_up(world):
    world.change_position_of(character=world._hero, add_y=-1)


def move_down(world):
    world.change_position_of(character=world._hero, add_y=1)


def move_left(world):
    world.change_position_of(character=world._hero, add_x=1)


def move_right(world):
    world.change_position_of(character=world._hero, add_x=-1)


def attack(world):
    world.process_attack(attacker=world._hero)


def pick_up(world):
    world.change_weapon(character=world._hero)
