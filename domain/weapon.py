from typing import NamedTuple


class Weapon(NamedTuple):
    damages: int
    auto_damages: int = 0

    def __str__(self):
        return 'W'
