# Toy RPG


This is a toy RPG game, compatible with Python 3.6.

Basic command line interface is available lauching
```
python main.py
```

# Domain tests

Basic unit testing for domain models can be launch with `python tests.py`
